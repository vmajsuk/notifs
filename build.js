import fs from 'fs';
import path from 'path';


const componentsDir = path.resolve(__dirname, 'server', 'components');
const templatesDir = path.resolve(__dirname, 'server', 'templates');

const componentsNames = fs.readdirSync(componentsDir);
const components = componentsNames.map(
    fname => ({ fname, fdata: fs.readFileSync(componentsDir + '/' + fname).toString() })
);

const templatesNames = fs.readdirSync(templatesDir);
const templates = templatesNames.map(
    fname => ({ fname, fdata: fs.readFileSync(templatesDir + '/' + fname).toString() })
);


function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function processFData(fdata) {
    return fdata.replace(
        /js\.import\(`[^`]+`(,\s?`{[^`]*}`)?\)/g,
        importStr => {
            let fname = importStr.split('`')[1];
            let props = importStr.split('`')[3];
            let rawFdata = components.find(c => c.fname === fname).fdata;

            if (props) {
    
                try {
                    props = JSON.parse(props);
                } catch (error) {
                    try {
                        eval(`props=${props}`);
                    } catch (err) {
                        return processFData(rawFdata);
                    }
                }

                Object.keys(props).forEach(
                    key => {
                        rawFdata = rawFdata.replace(new RegExp(escapeRegExp(key), 'g'), props[key]);
                    }
                );                
            }

            return processFData(rawFdata);
        }
    );
}

function build() {
    const builtTemplates = templates.map(
        tpl => ({
            fname: tpl.fname,
            fdata: processFData(tpl.fdata)
        })
    );

    const buildPath = path.resolve(__dirname, 'build');
    
    
    builtTemplates.forEach(
        async tpl => {
            // console.log(buildPath + '\\' + tpl.fname)

            fs.writeFileSync(buildPath + '\\' + tpl.fname, tpl.fdata)
        }
    )
}

build();

// console.log(minify(builtTemplates[0].fdata))

