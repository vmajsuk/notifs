var WebSocketServer = require('websocket').server;
var chokidar = require('chokidar');
var path = require('path');
var fs = require('fs');


var watcher = chokidar.watch();
watcher.add([
    path.resolve(__dirname, 'components'),
    path.resolve(__dirname, 'templates')
])

var connection;
var currentTemplatePath;

function readFilePromise (fname) {
    return new Promise((res, rej) => {
        fs.readFile(fname, (e, d) => e ? rej(e) : res(d.toString()))
    });
}

module.exports = server => {
    var wsServer = new WebSocketServer({
        httpServer: server,
        autoAcceptConnections: true
    });
    
    wsServer.on('connect', function(webSocketConnection) {
        connection = webSocketConnection;
        console.log('Client connected...');

        watcher.on('change', (path) => {
            let pathItems = path.split(/\/|\\/g);

            readFilePromise(path)
                .then(fdata => connection.sendUTF(JSON.stringify({
                    type: pathItems[pathItems.length - 2],
                    fname: pathItems[pathItems.length - 1],
                    fdata
                })));
        });

        connection.on('close', function(reasonCode, description) {
            console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
        });
    });
}