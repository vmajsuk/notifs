var fs = require('fs');
var path = require('path');

function readFilePromise (fname) {
    return new Promise((res, rej) => {
        fs.readFile(fname, (e, d) => e ? rej(e) : res(d.toString()))
    });
}

function readDirPromise (fname) {
    return new Promise((res, rej) => {
        fs.readdir(fname, (e, d) => e ? rej(e) : res(d))
    });
}

module.exports.static = function (request, response) {
    if (/\/\w+(\.\w+)+$/.test(request.url)) {
        fs.readFile(
            path.resolve(__dirname, './public' + request.url),
            (err, data) => {
                if (err) {
                    response.statusCode = 404;
                    response.end('');
                }

                response.end(data.toString());
            }
        );
        return 0;
    } else {
        return 1;
    }
}

module.exports.templates = function (request, response) {
    if (request.url === '/api/templates') {
        readDirPromise(path.resolve(__dirname, 'templates'))
            .then(fnames => Promise.all(
                fnames.map(
                    fname => readFilePromise(path.resolve(__dirname, 'templates', fname))
                        .then(fdata => ({ fname, fdata }))
                )
            ))
            .then(files => {
                response.setHeader('Content-Type', 'application/json');
                response.end(JSON.stringify({ templates: files }));
            });
        
        return 0;
    } else {
        return 1;
    }
}

module.exports.components = function (request, response) {
    if (request.url === '/api/components') {
        readDirPromise(path.resolve(__dirname, 'components'))
            .then(fnames => Promise.all(
                fnames.map(
                    fname => readFilePromise(path.resolve(__dirname, 'components', fname))
                        .then(fdata => ({ fname, fdata }))
                )
            ))
            .then(files => {
                response.setHeader('Content-Type', 'application/json');
                response.end(JSON.stringify({ components: files }));
            });
        
        return 0;
    } else {
        return 1;
    }
}
