var http = require('http');
var ws = require('./websocket');
var httpMw = require('./http');

function composeMws (...mws) {
    return function (request, response) {
        let i = 0,
        n = mws.length,
        res;
    
        for (;i < n; i++) {
            res = mws[i](request, response);

            if (res === 0) {
                return res;
            }
        }
    }
}

var server = http.createServer(
    function(request, response) {
        if (request.url === '/') {
            request.url = '/index.html';
        }

        var mwRes = composeMws
            (httpMw.static, httpMw.templates, httpMw.components)
            (request, response);

        if (mwRes !== 0) {
            response.end('ok', 'utf-8');
        }
    }
);

ws(server);

server.listen(1357, function() {
    console.log((new Date()) + ' Server is listening on port 8080');
});

