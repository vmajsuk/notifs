const socket = new WebSocket('ws://localhost:1357');

socket.addEventListener('open', function(event) {
    console.log('Opened websocket connection...')
});

// Listen for messages
socket.addEventListener('message', function(event) {
    const rawStr = event.data;
    const msg = JSON.parse(rawStr);
    const tpl = State[msg.type].find(t => t.fname === msg.fname);

    if (tpl) {
        tpl.fdata = msg.fdata;
        render(State.currentTemplate);
    }


});

// Php functions

window.strlen = s => s ? s.length : 0;

// Constants


let State = {
    data: {}
};

const root = document.getElementById('root');
const input = document.getElementById('input');
const notifRoot = document.getElementById('notif-root');
const notifMobileRoot = document.getElementById('notif-mobile-root');

input.onchange = e => {
    try {
        State.data = JSON.parse(e.target.value);
        input.value = JSON.stringify(State.data, null, 3);
        render(State.currentTemplate);
    } catch (err) {
        try {
            eval('State.data=' + e.target.value.replace('/\n|\r/', ''));
            input.value = JSON.stringify(State.data, null, 3);
            render(State.currentTemplate);
        } catch (error) {}
    }
}

State.root = root;
State.input = input;
State.notifRoot = notifRoot;
State.notifMobileRoot = notifMobileRoot;

Promise.all([
    fetch('/api/templates')
        .then(r => r.json()),
    fetch('/api/components')
        .then(r => r.json())
])
    .then(payload => {
        let templates = payload[0].templates,
            components = payload[1].components;

        State.templates = templates;
        State.data.templates = templates;
        State.components = components;
        State.data.components = components;
    })
    .then(() => render());

function render(templateName) {
    if (!State.root.firstChild) {
        State.root.innerHTML = `
        <div class="lists">
            <div class="pages">
                <ul>
                    ${State.templates.map(tpl => `<li onclick="to('${tpl.fname}')">${tpl.fname}</li>`)}
                </ul>
            </div>
            <div class="common-components">
                <ul>
                    ${State.components.map(tpl => `<li onclick="to('${tpl.fname}')">${tpl.fname}</li>`)}
                </ul>
            </div>
        </div>`;
    }

    if (templateName) {
        const isComponent = !!State.components.find(a => a.fname === templateName);
        const tpl = (
            State.templates.find(a => a.fname === templateName)
                || State.components.find(a => a.fname === templateName)
        ).fdata;
        const tplWithInlinedImports = isComponent
            ? processFData(`js.import(\`${templateName}\`, \`${JSON.stringify(State.data.props || {})}\`)`)
            : processFData(tpl);
            console.log(tplWithInlinedImports)
        const compiledTpl = new jSmart(tplWithInlinedImports);
        const markup = compiledTpl.fetch(Object.assign({}, State.data));
        // const iFrameSrc = 'data:text/html;charset=utf-8,' + encodeURIComponent(markup);

        State.notifRoot.srcdoc = markup;
        State.notifMobileRoot.srcdoc = markup;

    }
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
  }

function processFData(fdata) {
    return fdata.replace(
        /js\.import\(`[^`]+`(,\s?`{[^`]*}`)?\)/g,
        importStr => {
            let fname = importStr.split('`')[1];
            let props = importStr.split('`')[3];
            let rawFdata = State.components.find(c => c.fname === fname).fdata;

            if (props) {
    
                try {
                    props = JSON.parse(props);
                } catch (error) {
                    try {
                        eval(`props=${props}`);
                    } catch (err) {
                        return processFData(rawFdata);
                    }
                }

                Object.keys(props).forEach(
                    key => {
                        rawFdata = rawFdata.replace(new RegExp(escapeRegExp(key), 'g'), props[key]);
                    }
                );                
            }

            return processFData(rawFdata);
        }
    );
}

window.to = str => {
    socket.send(str);
    State.currentTemplate = str;
    render(str);
};
