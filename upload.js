import fs from 'fs';
import path from 'path';
import schema from 'schema-client';

// const client = new schema.Client('spinn-marketplace', 'R3EweENi1XZ0W0ItFItcHYm1jUuc2x3w');
const client = new schema.Client('spinn-marketplace-dev', 'UZXtJmRnfurzGTlWCjnFpSYYoTAwSSWH');

const highlightRoasterTemplate = fs.readFileSync(
    path.resolve(__dirname, 'build', 'roaster_hightlight.html')
).toString();

async function upload() {
    const highlightRoasterNotif = await client.get('/:notifications', { name: 'Highlight Roaster' })
        .then(r => r.results[0]);

    const response = await client.put('/:notifications/' + highlightRoasterNotif.id, {
        content: { html: highlightRoasterTemplate }
    });

    if (!response.errors) {
        console.log('templates uploaded');
    } else {
        console.log('Error!');
        console.log(response);
        return;
    }

}

upload();


